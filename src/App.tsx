import React from 'react';
import logo from './logo.svg';
import './App.css';
import Customer from './components/Customer';
import CustomerClass from './components/CustomerClass';
import Employee from './components/Employee';
import EmployeeClass from './components/EmployeeClass';
import Greeting from './components/Greeting';
import LoginForm from './components/Login/LoginForm';
import UserList from './components/user/UserList';
import UserDetail from './components/user/UserDetail';

import {Routes, Route} from 'react-router-dom'
function App() {
  return (
    <div className="App">
      {/* Customer
     <Customer name={"joyner"} age={31} title={"Title"}/>
     CustomerClass
     <CustomerClass  name={"joyner2"} age={31} title={"Title1"}/>
     Employee
     <Employee/>
     Employee class
     <EmployeeClass/> */}
     {/* <Greeting/> */}
     {/* <LoginForm/> */}
     {/* <UserList/> */}
     <Routes>
       <Route path={'/'} element={<UserList/>}/>
       <Route path={'/login'} element={<LoginForm/>}/>
       <Route path={'/user/:id'} element={<UserDetail/>}/>
     </Routes>
    </div>
  );
}

export default App;
