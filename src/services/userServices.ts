import axios from "axios";


class UserService{
 private static serverURL = "https://jsonplaceholder.typicode.com"

 public static getAllUsers(){
     let apiUrl = `${this.serverURL}/users`
     return axios.get(apiUrl)

 }

 public static getUser(userId:string){
    let apiUrl = `${this.serverURL}/users/${userId}`
    return axios.get(apiUrl)
 }
}


export default UserService