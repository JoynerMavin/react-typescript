import React from "react";

interface IProps{
    name: string;
    age: number;
    title : string;
}

let Customer:React.FC<IProps> = ({name, age, title})=>{
    console.log(name, age, title);
    return(
        <div>
            <div> Name: {name}</div>
            <div> Age: {age}</div>
            <div> Title :{title}</div>
        </div>
    )
};

export default Customer;