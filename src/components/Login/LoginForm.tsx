import React, { useState } from "react";

interface Iprops {

}

interface IState {
    user: {
        username: string;
        password: string;
    }

}

let LoginForm: React.FC = () => {

    let [state, setState] = useState<IState>({
        user: {
            username: '',
            password: ''
        }

    });


    let updateInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
        console.log("state.user");
        console.log(state.user);

        setState({
            user: {
                ...state.user,
                [event.target.name]: event.target.value
            }
        })
        console.log("state.user");
        console.log(state.user);

    };

    let login = (event:React.FormEvent<HTMLFormElement>):void =>{
        console.log("login")
        event.preventDefault();  // to stop loading
    }


    return (
        <div className="w-screen h-screen flex  flex-col justify-center items-center  bg-neutral-500">
            <div className="w-1/2 h-1/2  border-4 border-teal-400  flex  flex-col space-y-10  rounded-lg">

                <div className=" border-0 bg-cyan-500 text-3xl">
                    LoginForm
                </div>
                <form className=" space-y-10" onSubmit={login}>

                    <div className="flex justify-center  text-lg space-x-5 ">
                        <div className="border-0 align-baseline	flex items-end">
                            UserName
                        </div>
                        <div className="">
                            <input type="text" onChange={updateInput} value={state.user.username} placeholder="username" className="px-3 py-3 placeholder-slate-300 text-slate-600 relative bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring w-60" name="username" />
                        </div>
                    </div>


                    <div className="flex justify-center  text-lg space-x-5 ">
                        <div className="border-0 align-baseline	flex items-end">
                            Password
                        </div>
                        <div className="">
                            <input type="password" onChange={updateInput} value={state.user.password} placeholder="password" className="px-3 py-3 placeholder-slate-300 text-slate-600 relative bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring w-60" name="password" />
                        </div>
                    </div>


                    <div className="">
                        <button type="submit" className="focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">Signin</button>
                    </div>
                </form>




            </div>
        </div>

    )

}

export default LoginForm;