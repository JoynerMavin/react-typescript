import React from "react";

interface IProps {
    name: string;
    age: number;
    title: string;
}
interface IState {

}

class CustomerClass extends React.Component<any, any>{
    constructor(props: IProps) {
        super(props)
    }

    render() {
        let { name, age, title } = this.props
        return (
            <div>
                <div> Name: {name}</div>
                <div> Age: {age}</div>
                <div> Title :{title}</div>
            </div>
        )
    }
}

export default CustomerClass;