import React, { useState } from "react";

interface IProps {

}

interface IState {
    message:string;
}
let Greeting:React.FC<IProps> = () => {

    let [state, setState] = useState<IState>({
        message:"hello",
    });

    let changeMessage = (greet : string) :void =>{
        console.log('changeMessage');
        setState({
            message:greet
        });
        console.log(state.message)
    };
    return (
        <div>
            <div className="border-2 border-gray-900 bg-orange-500">
                {state.message}
            </div>
            <button type="button"  onClick={()=>changeMessage('mrng')} className="text-white bg-green-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800" >Mrng</button>
            <button type="button" onClick={()=>changeMessage('afteroon')} className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Afternoon</button>
            <button type="button" onClick={()=>changeMessage('evening')} className="text-white bg-purple-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Evening</button>

        </div>

    )
}

export default Greeting;