import React, { useState } from "react";

interface IProps {

}
interface IState {
    name: string;
    age: number;
    title: string;

}
let Employee: React.FC<IProps> = () => {
    let [state, setState] = useState<IState>(
        {
            name: 'raj',
            age: 12,
            title: 'title'
        }
    );
    return (
        <div>
            <div> Name: {state.name}</div>
            <div> Age: {state.age}</div>
            <div> Title :{state.title}</div>
        </div>
    )
}

export default Employee;