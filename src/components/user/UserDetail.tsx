import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { URLSearchParams } from "url";

import { IUser } from '../../model/IUser'
import UserService from "../../services/userServices";
interface IProps {

}

interface IState {
    loading: false;
    user: IUser;
    errorMessage: string;

}

let UserDetail: React.FC<IProps> = () => {

    // let {id} = useParams(); //working
    let { id } = useParams<URLSearchParams | any>();
    let [state, setState] = useState({
        loading: false,
        user: {} as IUser,
        errorMessage: ''
    });

    useEffect(() => {
        if (id) {
            UserService.getUser(id).then((response) => {
                console.log("ud:", response.data);
                if (response.data) {
                    setState({
                        ...state,
                        loading: false,
                        user: response.data,
                        errorMessage: ''

                    })

                    console.log(state);
                }
            }).catch(() => {

            });
        }
    }, [id])

    let { loading, user, errorMessage } = state;
    return (
        <div >
            <div className="flex flex-col w-1/2 border-2">UserDetail for {id}
                <div> ID: {user.id}   </div>
                <div> Name: {user.name}   </div>
                <div> Email: {user.email}   </div>
            </div>

        </div>
    )
}

export default UserDetail;