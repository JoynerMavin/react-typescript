import React, { useEffect, useState } from "react";
import { IUser } from "../../model/IUser";
import UserService from "../../services/userServices";
import { Link } from 'react-router-dom'

interface IProps {

}

interface IState {
    loading: boolean;
    users: IUser[];
    errorMessage: string;
}

let UserList: React.FC<IProps> = () => {

    let [state, setState] = useState<IState>({
        loading: false,
        users: [],
        errorMessage: ''
    });

    useEffect(() => {
        UserService.getAllUsers().then((response) => {
            // console.log(response.data)
            if (response.data) {
                setState({
                    ...state,
                    loading: false,
                    users: response.data
                })
            }
        }).catch((error) => {
            setState({
                ...state,
                loading: false,
                errorMessage: error
            })
        });

    }, []);
    let { loading, users, errorMessage } = state
    return (
        <div className="flex flex-col justfy-center space-y-6">
            <div>UserList</div>
            <div className="flex justify-center">
                <table className="table-fixed bg-slate-200 rounded-lg">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Company</th>
                            <th>Website</th>
                        </tr>
                    </thead>
                    <tbody className=" bg-neutral-600">
                        {
                            users.length > 0 && users.map(user => {
                                return (
                                    <tr key={user.id}>
                                        <td>{user.id}</td>
                                        <td><Link to={`user/${user.id}`}>{user.name} </Link></td>
                                        <td>{user.email}</td>
                                        <td>{user.phone}</td>
                                        <td>{user.company.name}</td>
                                        <td>{user.website}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>

        </div>
    )
}

export default UserList;