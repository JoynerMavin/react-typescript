import exp from "constants";
import React from "react";

interface IProps {

}

interface IState {
    name: string;
    age: number;
    title: string;
}

class EmployeeClass extends React.Component<any, any>{
    constructor(props: IProps) {
        super(props)
        this.state = {
            name: 'raj class',
            age: 1,
            title: 'title'
        } as IState
    }

    render() {
        let {name, age, title} = this.state;
        return (
            <div>
                <div> Name: {name}</div>
                <div> Age: {age}</div>
                <div> Title :{title}</div>
            </div>
        )
    }


}

export default EmployeeClass;